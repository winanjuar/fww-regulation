import {
  Body,
  Controller,
  HttpCode,
  Logger,
  Post,
  UseGuards,
} from '@nestjs/common';
import { AppService } from './app.service';
import { IdentityNumberDto } from './dto/identity-number.dto';
import { CivilDto } from './dto/civil.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller({ version: '1' })
export class AppController {
  private readonly logger = new Logger(AppController.name);
  constructor(private readonly appService: AppService) {}

  @UseGuards(AuthGuard('basic'))
  @HttpCode(200)
  @Post('check-imigrasi')
  async checkImigrasi(@Body() identityNumberDto: IdentityNumberDto) {
    this.logger.log('Checking imigrasi');
    return await this.appService.checkImigrasi(identityNumberDto);
  }

  @UseGuards(AuthGuard('basic'))
  @HttpCode(200)
  @Post('check-peduli-lindungi')
  async checkPeduliLindungi(@Body() identityNumberDto: IdentityNumberDto) {
    this.logger.log('Checking peduli lindungi');
    return await this.appService.checkPeduliLindungi(identityNumberDto);
  }

  @UseGuards(AuthGuard('basic'))
  @HttpCode(200)
  @Post('check-disdukcapil')
  async checkDisdukcapil(@Body() civilDto: CivilDto) {
    this.logger.log('Checking disdukcapil');
    return await this.appService.checkDisDukCapil(civilDto);
  }
}
