import { Injectable, Logger } from '@nestjs/common';
import { MatchDisDukCapil } from 'src/entity/match-disdukcapil.entity';
import { DataSource, Repository } from 'typeorm';

@Injectable()
export class MatchDisDukCapilRepository extends Repository<MatchDisDukCapil> {
  private readonly logger = new Logger(MatchDisDukCapilRepository.name);
  constructor(dataSource: DataSource) {
    super(MatchDisDukCapil, dataSource.createEntityManager());
  }

  async findOneByIdentityNumber(
    identityNumber: string,
  ): Promise<MatchDisDukCapil> {
    this.logger.log('Get data from DB');
    return await this.findOneBy({ identityNumber });
  }
}
