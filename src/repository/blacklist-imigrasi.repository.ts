import { Injectable, Logger } from '@nestjs/common';
import { IdentityNumberDto } from 'src/dto/identity-number.dto';
import { BlackListImigrasi } from 'src/entity/blacklist-imigrasi.entity';
import { DataSource, Repository } from 'typeorm';

@Injectable()
export class BlackListImigrasiRepository extends Repository<BlackListImigrasi> {
  private readonly logger = new Logger(BlackListImigrasiRepository.name);
  constructor(dataSource: DataSource) {
    super(BlackListImigrasi, dataSource.createEntityManager());
  }

  async findOneByIdentityNumber(
    identityNumberDto: IdentityNumberDto,
  ): Promise<BlackListImigrasi> {
    this.logger.log('Get data from DB');
    return await this.findOneBy(identityNumberDto);
  }
}
