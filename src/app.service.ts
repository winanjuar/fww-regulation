import { Injectable, Logger } from '@nestjs/common';
import { BlackListImigrasiRepository } from './repository/blacklist-imigrasi.repository';
import { IdentityNumberDto } from './dto/identity-number.dto';
import { IResponseImigrasi } from './interface/response-imigrasi.interface';
import { UnBoosterPeduliLindungiRepository } from './repository/unbooster-pedulilindungi.repository';
import { IResponsePeduliLindungi } from './interface/response-pedulilindungi.interface';
import { MatchDisDukCapilRepository } from './repository/match-disdukcapil.repository';
import { CivilDto } from './dto/civil.dto';
import { IResponseDisdukcapil } from './interface/response-disdukcapil.interface';

@Injectable()
export class AppService {
  private readonly logger = new Logger(AppService.name);
  constructor(
    private readonly blackListImigrasiRepository: BlackListImigrasiRepository,
    private readonly unboosterPeduliLindungi: UnBoosterPeduliLindungiRepository,
    private readonly matchDisDukCapilRepository: MatchDisDukCapilRepository,
  ) {}

  async checkImigrasi(
    identityNumberDto: IdentityNumberDto,
  ): Promise<IResponseImigrasi> {
    const dataFound =
      await this.blackListImigrasiRepository.findOneByIdentityNumber(
        identityNumberDto,
      );

    if (dataFound) {
      this.logger.log('Result checking migrasi prohibited to fly');
      return {
        identityNumber: identityNumberDto.identityNumber,
        allowedToFly: false,
        reason: dataFound.reason,
      } as IResponseImigrasi;
    }

    this.logger.log('Result checking imigrasi allowed to fly');
    return {
      identityNumber: identityNumberDto.identityNumber,
      allowedToFly: true,
      reason: null,
    } as IResponseImigrasi;
  }

  async checkPeduliLindungi(
    identityNumberDto: IdentityNumberDto,
  ): Promise<IResponsePeduliLindungi> {
    const dataFound =
      await this.unboosterPeduliLindungi.findOneByIdentityNumber(
        identityNumberDto,
      );

    if (dataFound) {
      this.logger.log('Result checking peduli lindungi should attach PCR');
      return {
        identityNumber: identityNumberDto.identityNumber,
        vaksinCovid: dataFound.vaksin,
        shouldPCR: true,
      } as IResponsePeduliLindungi;
    }

    this.logger.log('Result checking imigrasi prohibited to fly');
    return {
      identityNumber: identityNumberDto.identityNumber,
      vaksinCovid: 3,
      shouldPCR: false,
    } as IResponsePeduliLindungi;
  }

  async checkDisDukCapil(civilDto: CivilDto): Promise<IResponseDisdukcapil> {
    const dataFound =
      await this.matchDisDukCapilRepository.findOneByIdentityNumber(
        civilDto.identityNumber,
      );

    if (dataFound) {
      if (
        civilDto.name.toUpperCase().trim() ===
          dataFound.name.toUpperCase().trim() &&
        civilDto.birthDate === String(dataFound.birthDate)
      ) {
        this.logger.log('Result checking disdukcapil real match');
        return {
          identityNumber: civilDto.identityNumber,
          match: true,
        } as IResponseDisdukcapil;
      } else {
        this.logger.log('Result checking disdukcapil unmatch');
        return {
          identityNumber: civilDto.identityNumber,
          match: false,
        } as IResponseDisdukcapil;
      }
    }

    this.logger.log('Result checking disdukcapil default match');
    return {
      identityNumber: civilDto.identityNumber,
      match: true,
    } as IResponseDisdukcapil;
  }
}
