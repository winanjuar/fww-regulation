import { IsDateString, IsNotEmpty, IsString, Length } from 'class-validator';

export class CivilDto {
  @IsString()
  @Length(16)
  @IsNotEmpty()
  identityNumber: string;

  @IsString()
  @IsNotEmpty()
  name: string;

  @IsDateString()
  @IsNotEmpty()
  birthDate: string;
}
