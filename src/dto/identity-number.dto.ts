import { PickType } from '@nestjs/mapped-types';
import { CivilDto } from './civil.dto';

export class IdentityNumberDto extends PickType(CivilDto, ['identityNumber']) {}
