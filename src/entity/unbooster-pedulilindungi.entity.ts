import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity('unbooster_pedulilindungi')
export class UnBoosterPeduliLindungi {
  @PrimaryColumn({ name: 'identity_number', length: 16 })
  identityNumber: string;

  @Column({ type: 'smallint' })
  vaksin: number;
}
