import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity('blacklist_imigrasi')
export class BlackListImigrasi {
  @PrimaryColumn({ name: 'identity_number', length: 16 })
  identityNumber: string;

  @Column({ length: 50 })
  reason: string;
}
