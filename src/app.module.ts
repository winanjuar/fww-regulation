import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { LoggerModule } from 'nestjs-pino';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BlackListImigrasi } from './entity/blacklist-imigrasi.entity';
import { MatchDisDukCapil } from './entity/match-disdukcapil.entity';
import { UnBoosterPeduliLindungi } from './entity/unbooster-pedulilindungi.entity';
import { BlackListImigrasiRepository } from './repository/blacklist-imigrasi.repository';
import { UnBoosterPeduliLindungiRepository } from './repository/unbooster-pedulilindungi.repository';
import { MatchDisDukCapilRepository } from './repository/match-disdukcapil.repository';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    LoggerModule.forRoot({
      pinoHttp: {
        formatters: {
          level: (label) => {
            return { level: label.toUpperCase() };
          },
        },
        customLevels: {
          emergerncy: 80,
          alert: 70,
          critical: 60,
          error: 50,
          warn: 40,
          notice: 30,
          info: 20,
          debug: 10,
        },
        useOnlyCustomLevels: true,
        transport: {
          target: 'pino-pretty',
          options: {
            singleLine: true,
            colorize: true,
            levelFirst: true,
            translateTime: 'SYS:standard',
            ignore: 'hostname,pid',
          },
        },
      },
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        type: 'mariadb',
        host: configService.get<string>('DB_HOST'),
        port: configService.get<number>('DB_PORT'),
        username: configService.get<string>('DB_USER'),
        password: configService.get<string>('DB_PASS'),
        database: configService.get<string>('DB_NAME'),
        entities: [
          BlackListImigrasi,
          MatchDisDukCapil,
          UnBoosterPeduliLindungi,
        ],
        synchronize: configService.get<boolean>('DB_SYNC') || false,
        dateStrings: true,
      }),
    }),
    AuthModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    BlackListImigrasiRepository,
    UnBoosterPeduliLindungiRepository,
    MatchDisDukCapilRepository,
  ],
})
export class AppModule {}
