export interface IResponsePeduliLindungi {
  identityNumber: string;
  vaksinCovid: number;
  shouldPCR: boolean;
}
