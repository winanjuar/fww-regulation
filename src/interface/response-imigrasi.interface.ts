export interface IResponseImigrasi {
  identityNumber: string;
  allowedToFly: boolean;
  reason: string | null;
}
